.PHONY: rel test

all:clean get-deps compile

compile:
	rebar compile

get-deps:
	rebar get-deps

clean:
	rebar clean
#   rm -f ebin/*


rel: all
	rm -rf rel/petri_net
	rm -f rel/reltool.config
	cp rel/reltool.config.prod rel/reltool.config
	rebar generate overlay_vars=vars/prod_vars.config

# test: all
#   # rm -rf test_rel/smsfc
#   # mkdir -p test_rel/smsfc
#   # rm -f priv/configs/env.config
#   # ln -s test.config priv/configs/env.config
#   # rm -f rel/reltool.config
#   # ln -s reltool.config.test rel/reltool.config
#   # ./rebar generate target_dir=../test_rel/smsfc overlay_vars=vars/test_vars.config
#   ./test_rel/starter.sh
#   ./rebar ct
#   # rm -rf .eunit
#   # ./rebar eunit
#   ./test_rel/smsfc/bin/smsfc stop

# test-ct:
#   ./rebar ct
test:
	rebar clean compile eunit
